# example test with Deno

When developing with vanilla javascript, there are situations when you want to test your code. Especially when it becomes not so trivial and complex.

## prerequisites
You need to have Deno installed: [installation instructions](https://deno.land/manual@v1.6.3/getting_started/installation)

## how to
You can run the example from a code editor like WebStorm or to run it from docker.

Windows (command prompt):
```shell
docker run -it -p 8080:80 -v %CD%:/usr/share/nginx/html  nginx
```

Linux:
```shell
docker run -it -p 8080:80 -v $PWD:/usr/share/nginx/html  nginx
```

You can access the demo from: http://localhost:8080/calculations.html

## to test with deno
It is simple:

```shell
deno test
```

To see how deno test respond to failures, just change the  [deno test file](./deno.library.test.js).

