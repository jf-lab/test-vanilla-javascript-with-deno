import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { createMultiplicationResults, sum, showAlert} from "./library.js";

Deno.test('4+5=9', () => {
  assert(sum(4,5) === '4 + 5 = 9')
})

Deno.test('table gives expected value', () => {
  let results = createMultiplicationResults(5);
  let result = results[2];

  assertEquals(result,  { 'x': 3, 'y': 5, 'prod': 15 });
})

Deno.test('test my alert', () => {
  window.alert = a => assert(a === 'test'); // to create a mock is fairly easy
  showAlert("test");
})