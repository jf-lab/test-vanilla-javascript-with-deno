"use strict";

function createMultiplicationResults(value) {
  let table = [];
  for (let i = 0; i < 10; i++) {
    let item = {'x': i + 1, 'y': value, 'prod': (i + 1) * value};
    table.push(item)
  }
  return table;
}

function sum(x, y) {

  return `${x} + ${y} = ${x + y}`;
}

function showAlert(valueToShow) {
  window.alert(valueToShow);
}

export {createMultiplicationResults, sum, showAlert}